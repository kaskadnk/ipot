<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    protected $fillable=['id','id_reg','id_user','id_quest','answer'];
}
