<?php

namespace App\Http\Controllers;

Use Auth;
Use App\Profile;
Use App\user_role;
Use App\bank_quest;
Use App\bank;
Use App\buffer;
Use App\zastroys;
Use App\zhks;
Use App\list_clients;
use App\Client;
use App\Questions;
use App\User;
use ZendPdf\PdfDocument;
use ZendPdf\Font;
use ZendPdf\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\UserProfile;
use Carbon\Carbon;

class UserProfile extends Controller
{
    
  public function index() {
        
       
        $roles = user_role::where('user_id','=',Auth::id())->get();
      foreach ($roles as $role){
       //   dd($role->role_id);
        return view('admin.layout',['role'=>$role]);
      }
    }
    public function create() {
        $item = bank::all();
        $zhks = zhks::all();
        $zastroys = zastroys::all();
        //dd($item);
        return view('add', ['item'=>$item,'zhks'=>$zhks,'zastroys'=>$zastroys]);
    }
    public function store(Request $request) {
        $item = new Profile();
        $item ->name =  $request->name;
        $item -> save();
        
    }
    public function edit($id) {
        
        $item = Profile::where('id', '=', $id)->get();
        //$item = Profile::where('id','=','$id')->get();
        return view('edit.step1', $item);
    }
    public function update($id, Request $request) {
        
        $item = Profile::firstOrNew (['id' => $id]);
        $item ->name =  $request->name;
        $item -> save();
        //$item = Profile::where('id','=','$id')->get();
        
    }
    public function list_clients(){
      //  dd(DB::table('id')->getNextGeneratedId());
//        $quest= bank_quest::where('id_bank','=',2)->get();
//        $x=255;
//        
//        foreach ($quest as $q){
//            $tr = new bank_quest();
//            $tr->id_bank = 2;
//            $tr->id_quest = $q->id_quest+$x;
//            $tr->x = $q->x;
//            $tr->y = $q->y;
//            $tr->type = $q->type;
//            $tr->value = $q->value;
//            $tr->page = $q->page+4;
//            $tr->save();
//            
//        }
//        $quest= Questions::all();
//        $x=86;
//        foreach ($quest as $q){
//            $tr = new Questions();
//            $tr->position = $x;
//            $tr->quest = $q->quest . ' созаемщика';
//            $tr->type = $q->type;
//            $tr->answer = $q->answer;
//            $tr->dop = 1;
//            $tr->save();
//            $x++;
//        }
        
        $clients = list_clients::where('id_reg',Auth::id())
                   ->where('pub','=',1)
                ->orderBy('id')
                   ->get();
        
//        $clients = [];
//        $currentUserId = 0;
//        foreach ($rawClients as $rawClientKey => $rawClient){
////            $clients[$rawClient->id_user] = '';
//            if ($currentUserId == $rawClient->id_user) {
//                $clients[$rawClient->id_user] .= $rawClient->answer . ' ';
//            } else {
//                $currentUserId = $rawClient->id_user;
//                $clients[$rawClient->id_user] = substr($clients[$rawClient->id_user], 0, -1);
//            }   
//        }
//        dd($clients);
//                  <td>{{$client->zhk}}</td>
//                  <td>{{$client->zastroy}}</td>
//                  <td>{{$client->comment}}</td>
       return view('client.list',['clients'=>$clients]); 
    }
    
     public function search_clients(Request $request){
      //   dd($request->search);
//        $quest= bank_quest::where('id_bank','=',4)->get();
//        $x=255;
//        
//        foreach ($quest as $q){
//            $tr = new bank_quest();
//            $tr->id_bank = 4;
//            $tr->id_quest = $q->id_quest+$x;
//            $tr->x = $q->x;
//            $tr->y = $q->y;
//            $tr->type = $q->type;
//            $tr->value = $q->value;
//            $tr->page = $q->page+6;
//            $tr->save();
//            
//        }
//        $quest= Questions::all();
//        $x=86;
//        foreach ($quest as $q){
//            $tr = new Questions();
//            $tr->position = $x;
//            $tr->quest = $q->quest . ' созаемщика';
//            $tr->type = $q->type;
//            $tr->answer = $q->answer;
//            $tr->dop = 1;
//            $tr->save();
//            $x++;
//        }
        $id_quest = [1, 2, 3];
        $search= Client::where('answer',  'like', '%' . $request->search. '%')->get();
        $clientId=[];
        foreach ($search as $_){
            $clientId[]=$_->id_user;
            
        }
       // dd($clientId);
        $clients = Client::whereIn('id_user', $clientId)
                    ->whereIn('id_quest', $id_quest)
                    ->where('id_reg',Auth::id())
                   ->orderBy('id_user')
                   ->orderBy('id_quest')
                   ->get();
        
//        $clients = [];
//        $currentUserId = 0;
//        foreach ($rawClients as $rawClientKey => $rawClient){
////            $clients[$rawClient->id_user] = '';
//            if ($currentUserId == $rawClient->id_user) {
//                $clients[$rawClient->id_user] .= $rawClient->answer . ' ';
//            } else {
//                $currentUserId = $rawClient->id_user;
//                $clients[$rawClient->id_user] = substr($clients[$rawClient->id_user], 0, -1);
//            }   
//        }
//        dd($clients);
//                  <td>{{$client->zhk}}</td>
//                  <td>{{$client->zastroy}}</td>
//                  <td>{{$client->comment}}</td>
       return view('client.list',['clients'=>$clients]); 
    }
    
     public function list_users(){
         $users=User::all();
        return view('user.list',['users'=>$users]);
    }
    public function add_user(Request $request){
        
        if ($request->login<>''){
            $reg = new User();
                $reg->name = $request->login;
                $reg->email = $request->email;
                $reg->firstname = $request->firstname;
                $reg->lastname = $request->lastname;
                $reg->sirname = $request->sirname;
                $reg->dolzh = $request->dolzh;
                $reg->partner = $request->partner;
                $reg->phone = $request->phone;
                $reg->password = bcrypt($request->password);
                $reg->save();
            $users=User::where('name','=',$request->login)->get();
           foreach ($users as $user) 
            $iduser=$user->id;
            
            $reg = new user_role();
            $reg->user_id = $iduser;
            $reg->role_id = $request->role;
            $reg->save();
            $users=User::all();
        return view('user.list',['users'=>$users]);
        } else {
            return view('user.add');
        }
    }
    
    public function edit_profil(Request $request){
        
        if ($request->login<>''){
            $reg = User::find(Auth::id());
                $reg->name = $request->login;
                $reg->email = $request->email;
                $reg->firstname = $request->firstname;
                $reg->lastname = $request->lastname;
                $reg->sirname = $request->sirname;
                $reg->dolzh = $request->dolzh;
                $reg->partner = $request->partner;
                $reg->phone = $request->phone;
                if ($request->password!='')
                $reg->password = bcrypt($request->password);
                $reg->save();
           
            $clients= Client::all();
        return view('client.list',['clients'=>$clients]);
        } else {
            $id=Auth::id();
            $users=User::where('id','=', $id )->get();
          // dd($user);
            return view('profile',['users'=>$users]);
        }
    }
   
     
     function translit($str) {
    $rus = array('А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я');
    $lat = array('A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I', 'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', 'Y', 'Y', 'Y', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e', 'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'sch', 'y', 'y', 'y', 'e', 'yu', 'ya');
    return str_replace($rus, $lat, $str);
  }
     public function get_vtb( Request $request){
      //  dd($_POST);
         $id = $request->id;
      $item = Client::where('id_user','=', $id)->get();
      //$quest = bank_quest::where('bank','=', 1)->get();
      $clients = list_clients::where('id','=',$id)->get();
      foreach ($clients as $client){
          $FIO=$client->FIO;
      }
      $FIO = $this->translit($FIO);
      
        $filename = $FIO . ".pdf";
        $pdf = PdfDocument::load('pdf/anket.pdf');
 

//$page->drawText($item->lasttname, 175, 725, 'UTF-8');

foreach ($item as $name) {
    $quest = bank_quest::where('id_bank','=', 1)->where('id_quest','=', $name->id_quest)->get();
    
     foreach ($quest as $que){
         
         if ($que->type=='string'){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
              if (!empty($name->answer)){
                   if (strlen($name->answer)>$que->perenos) {
           $answer = $name->answer;
          $answerstr =substr($answer,0,$que->perenos);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  
         $page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
         $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
       } else {
             
           //  dd($name->answer);
             $page->drawText($name->answer, $que->x, $que->y, 'UTF-8');
       } }}
         if ($que->type=='date'){
             if (!empty($name->answer)){
                 if (strlen($name->answer)==10){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             //dd($name->answer);
             list($year,$month,$day) = explode('-',$name->answer);
             
             $answer = $day . '.' . $month . '.' . $year;
             $page->drawText($answer, $que->x, $que->y, 'UTF-8');
                 }}}
         if ($que->type=='addressfactsoz'){
            if (!empty($name->answer)){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $answer='';
             
             $addressquest=[332,333,334,335,336,337,338,339,340];
             $addressQuery= Client::where('id_user','=',$id)->whereIn('id_quest',$addressquest)->orderBy('id_quest')->get();
             foreach ($addressQuery as $address){
                 if ($address->answer!=''){
                 if ($answer==''){
                         
                      $answer = $address->answer;   
                     } else {
                      $answer = $answer . ', ' . $address->answer;    
                     }
             }
             }
             if (strlen($answer)>$que->perenos) {
           
          $answerstr =substr($answer,0,$que->perenos);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  
         $page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
         $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
       }
           //  $answer = $item[31]->answer . ', ' . $item[32]->answer . ', ' . $item[33]->answer . ', ' . $item[34]->answer . ', ' . $item[35]->answer . ', ' . $item[36]->answer . ', ' . $item[37]->answer . ', ' . $item[38]->answer . ', ' . $item[39]->answer;
            //  dd($xcoord);
           //  $page->drawText($answer, $que->x, $que->y, 'UTF-8');
         }}
             if ($que->type=='addressfact'){
            if (!empty($name->answer)){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $answer='';
             
             $addressquest=[77,78,79,80,81,82,83,84,85];
             $addressQuery= Client::where('id_user','=',$id)->whereIn('id_quest',$addressquest)->orderBy('id_quest')->get();
             foreach ($addressQuery as $address){
                 if ($address->answer!=''){
                 if ($answer==''){
                         
                      $answer = $address->answer;   
                     } else {
                      $answer = $answer . ', ' . $address->answer;    
                     }
             }
             }
    
       if (strlen($answer)>$que->perenos) {
           
          $answerstr =substr($answer,0,$que->perenos);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  
         $page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
         $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
       }
           //  $answer = $item[31]->answer . ', ' . $item[32]->answer . ', ' . $item[33]->answer . ', ' . $item[34]->answer . ', ' . $item[35]->answer . ', ' . $item[36]->answer . ', ' . $item[37]->answer . ', ' . $item[38]->answer . ', ' . $item[39]->answer;
            //  dd($xcoord);
           //  $page->drawText($answer, $que->x, $que->y, 'UTF-8');
         }}
         if ($que->type=='addressreg'){
            
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $answer='';
             $addressquest=[68,69,70,71,72,73,74,75,76];
             $addressQuery= Client::where('id_user','=',$id)->whereIn('id_quest',$addressquest)->orderBy('id_quest')->get();
             foreach ($addressQuery as $address){
                 if ($address->answer!=''){
                 if ($answer==''){
                         
                      $answer = $address->answer;   
                     } else {
                      $answer = $answer . ', ' . $address->answer;    
                     }
             }
             }
//       
           //  $answer = $item[31]->answer . ', ' . $item[32]->answer . ', ' . $item[33]->answer . ', ' . $item[34]->answer . ', ' . $item[35]->answer . ', ' . $item[36]->answer . ', ' . $item[37]->answer . ', ' . $item[38]->answer . ', ' . $item[39]->answer;
            //  dd($xcoord);
             //dd(explode(',',$answer,3));
             
             //dd($answer);
             $page->drawText($answer, 60, 692, 'UTF-8');
         }
         if ($que->type=='quad'){
              if (!empty($name->answer)){
             $rt = preg_split('//u',$name->answer,-1,PREG_SPLIT_NO_EMPTY);
             $table=$que->value;
             $x=$que->x;
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
         }}
          if ($que->type=='check'){
         if ($que->value<>''){
             if ($que->value==$name->answer){
                 $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText('v', $que->x, $que->y, 'UTF-8');
             
             }
         } else {
             
         }
         }
     }
    
//   $page->drawText($name->answer, 175, $height, 'UTF-8');
//   $height = $height - 10;
}
//$page->drawText($item->lasttname, 175, 725, 'UTF-8');
//$page->drawText($item->firstname, 175, 715, 'UTF-8');
//$page->drawText($item->sirtname, 175, 705, 'UTF-8');
$pdf->save('zend.pdf');
return \Response::make($pdf->render(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
    public function get_spb( Request $request){
      //  dd($_POST);
         $id = $request->id;
      $item = Client::where('id_user','=', $id)->get();
      //$quest = bank_quest::where('bank','=', 1)->get();
       $clients = list_clients::where('id','=',$id)->get();
      foreach ($clients as $client){
          $FIO=$client->FIO;
      }
      $FIO = $this->translit($FIO);
      
        $filename = $FIO . ".pdf";
       
        $pdf = PdfDocument::load('pdf/spb.pdf');
        $font = Font::fontWithPath('arial.ttf');
$page = $pdf->pages[0];

$page->setFont($font, 10);

//$page->drawText($item->lasttname, 175, 725, 'UTF-8');

foreach ($item as $name) {
    $quest = bank_quest::where('id_bank','=', 2)->where('id_quest','=', $name->id_quest)->get();
     foreach ($quest as $que){
         if ($que->type=='date'){
              if (!empty($name->answer)){
                   if (strlen($name->answer=10)){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             list($year,$month,$day) = explode('-',$name->answer);
             $answer = $day . '.' . $month . '.' . $year;
             $page->drawText($answer, $que->x, $que->y, 'UTF-8');
                   }}}
         if ($que->type=='string'){
              if (!empty($name->answer)){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $answer = $name->answer;
             //$page->drawText($name->answer, $que->x, $que->y, 'UTF-8');
           if (strlen($answer)>$que->perenos) {
           
          $answerstr =substr($answer,0,$que->perenos);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  
         $page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
         if ($que->dopx!=0){
          $page->drawText(substr($answer,$ind), $que->dopx, $que->y-$que->dop, 'UTF-8');   
         } else {
         $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
         }
       }
              else {
                 $page->drawText($answer, $que->x, $que->y, 'UTF-8');
             }
         }}
          if ($que->type=='check'){
         if ($que->value<>''){
             if ($que->value==$name->answer){
                 $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText('v', $que->x, $que->y, 'UTF-8');
             
             }
         } else {
             
         }
         }
     }
     
    
//   $page->drawText($name->answer, 175, $height, 'UTF-8');
//   $height = $height - 10;
}
//$page->drawText($item->lasttname, 175, 725, 'UTF-8');
//$page->drawText($item->firstname, 175, 715, 'UTF-8');
//$page->drawText($item->sirtname, 175, 705, 'UTF-8');
$pdf->save('zend.pdf');
return \Response::make($pdf->render(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
    
     public function get_sber( Request $request){
      //  dd($_POST);
         $id = $request->id;
      $item = Client::where('id_user','=', $id)->get();
      //$quest = bank_quest::where('bank','=', 1)->get();
       $clients = list_clients::where('id','=',$id)->get();
      foreach ($clients as $client){
          $FIO=$client->FIO;
      }
      $FIO = $this->translit($FIO);
      
        $filename = $FIO . ".pdf";

        $pdf = PdfDocument::load('pdf/sber2.pdf');

        $font = Font::fontWithPath('arial.ttf');
$page = $pdf->pages[0];

$page->setFont($font, $que->font);

//$page->drawText($item->lasttname, 175, 725, 'UTF-8');



foreach ($item as $name) {
    $quest = bank_quest::where('id_bank','=', 3)->where('id_quest','=', $name->id_quest)->get();
     foreach ($quest as $que){
         
         if ($que->type=='quad'){
             if (!empty($name->answer)){ 
                 $table=$que->value;
             $x=$que->x;
             $answer= mb_strtoupper($name->answer);
               if ($que->perenos!=0) {
          // dd($table);
                  // dd($answer);
          $answerstr =substr($answer,0,30);
         // dd($answerstr);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  $rt = preg_split('//u',$answerstr,-1,PREG_SPLIT_NO_EMPTY);
             //dd($rt);
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
          $answerstr =substr($answer,$ind); 
          //dd($answerstr);
          $x=$que->dopx;
           $rt = preg_split('//u',$answerstr,-1,PREG_SPLIT_NO_EMPTY);
           // dd($rt);
           
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y-$que->dop, 'UTF-8');
             $x=$x+$table+0.1;
             }
         //$page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
//         if ($que->dopx!=0){
//          $page->drawText(substr($answer,$ind), $que->dopx, $que->y-$que->dop, 'UTF-8');   
//         } else {
       //  $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
//         }
       } else {
             $rt = preg_split('//u',$answer,-1,PREG_SPLIT_NO_EMPTY);
           //dd($rt);
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
             }}}
         if ($que->type=='phonesber'){
              if (!empty($name->answer)) {
                   if (strlen($name->answer)==16){
                  
             $answer = mb_substr($name->answer,3);
             //dd($answer);
             //dd($answer);
             $answer = str_replace('-','',$answer);
            list($code, $number) =  explode(')', $answer);
            // $a = explode(')', $answer);
            
             //dd($day, $month);
             
             $rt = preg_split('//u',$code,-1,PREG_SPLIT_NO_EMPTY);
             $table=$que->value;
             $x=$que->x;
             foreach ($rt as $kod){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($kod, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
             $rt = preg_split('//u',$number,-1,PREG_SPLIT_NO_EMPTY);
             $table=$que->value;
             $x=$que->x+$que->dop;
             foreach ($rt as $kod){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($kod, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
              }}
         }
         
         if ($que->type=='date'){
              if (!empty($name->answer)){
                  if (strlen($name->answer)==10){
             $rt = preg_split('/\./',$name->answer,-1,PREG_SPLIT_NO_EMPTY);
//             dd($name->answer[6] . $name->answer[7] . $name->answer[8] . $name->answer[9]);
             list($year, $month, $day) =  explode('-', $name->answer);
            // dd($day, $month, $year);
//             $rt = explode('.', $name->answer);
//             dd($rt[2][]);
            // dd($rt);
             
             $table=$que->value;
             $x=$que->x;
             $page->drawText($day[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($day[1], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($month[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($month[1], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($year[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[1], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[2], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[3], $x, $que->y, 'UTF-8');
             $x=$x+$table;
//             foreach ($rt as $word){
//             $page = $pdf->pages[$que->page];
//             $font = Font::fontWithPath('arial.ttf');
//             $page->setFont($font, 10);
//             $page->drawText($word, $x, $que->y, 'UTF-8');
//             $x=$x+$table+0.1;
//             }
         }}}
         if ($que->type=='check'){
         if ($que->value<>''){
             if ($que->value==$name->answer){
                 $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText('v', $que->x, $que->y, 'UTF-8');
             
             }
         } else {
             
         }
         }
     }
    
//   $page->drawText($name->answer, 175, $height, 'UTF-8');
//   $height = $height - 10;
}
//$page->drawText($item->lasttname, 175, 725, 'UTF-8');
//$page->drawText($item->firstname, 175, 715, 'UTF-8');
//$page->drawText($item->sirtname, 175, 705, 'UTF-8');
$pdf->save('zend.pdf');
return \Response::make($pdf->render(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
    
         public function get_prom( Request $request){
      //  dd($_POST);
         $id = $request->id;
      $item = Client::where('id_user','=', $id)->get();
      //$quest = bank_quest::where('bank','=', 1)->get();
       $clients = list_clients::where('id','=',$id)->get();
      foreach ($clients as $client){
          $FIO=$client->FIO;
      }
      $FIO = $this->translit($FIO);
      
        $filename = $FIO . ".pdf";
      
        $pdf = PdfDocument::load('pdf/psb2.pdf');
        $font = Font::fontWithPath('arial.ttf');
$page = $pdf->pages[0];

$page->setFont($font, $que->font);

//$page->drawText($item->lasttname, 175, 725, 'UTF-8');

foreach ($item as $name) {
    $quest = bank_quest::where('id_bank','=', 4)->where('id_quest','=', $name->id_quest)->get();
     foreach ($quest as $que){
         
         if ($que->type=='quad'){
             if (!empty($name->answer)){ 
                 $table=$que->value;
             $x=$que->x;
             $answer= mb_strtoupper($name->answer);
               if ($que->perenos!=0) {
          // dd($table);
                  // dd($answer);
          $answerstr =substr($answer,0,$que->perenos);
         // dd($answerstr);
          $ind=strripos($answerstr,' ');
          $answerstr =substr($answer,0,$ind);
                  $rt = preg_split('//u',$answerstr,-1,PREG_SPLIT_NO_EMPTY);
             //dd($rt);
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
          $answerstr =substr($answer,$ind); 
          //dd($answerstr);
          $x=$que->dopx;
           $rt = preg_split('//u',$answerstr,-1,PREG_SPLIT_NO_EMPTY);
           // dd($rt);
           
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y-$que->dop, 'UTF-8');
             $x=$x+$table+0.1;
             }
         //$page->drawText($answerstr, $que->x, $que->y, 'UTF-8');
//         if ($que->dopx!=0){
//          $page->drawText(substr($answer,$ind), $que->dopx, $que->y-$que->dop, 'UTF-8');   
//         } else {
       //  $page->drawText(substr($answer,$ind), $que->x, $que->y-$que->dop, 'UTF-8');
//         }
       } else {
             $rt = preg_split('//u',$answer,-1,PREG_SPLIT_NO_EMPTY);
           //dd($rt);
             foreach ($rt as $word){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($word, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
             }}}
         if ($que->type=='phoneprom'){
              if (!empty($name->answer)){
                  if (strlen($name->answer)==16){
             $answer = mb_substr($name->answer,3);
             //dd($answer);
             list($code, $number) =  explode(')', $answer);
             //dd($day, $month);
             $number = str_replace('-','',$number);
             $rt = preg_split('//u',$code,-1,PREG_SPLIT_NO_EMPTY);
             $table=$que->value;
             $x=$que->x;
             foreach ($rt as $kod){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($kod, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
             $rt = preg_split('//u',$number,-1,PREG_SPLIT_NO_EMPTY);
             $table=$que->value;
             $x=$que->x+$que->dop;
             foreach ($rt as $kod){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText($kod, $x, $que->y, 'UTF-8');
             $x=$x+$table+0.1;
             }
              }}
         }
         if ($que->type=='snils'){
              if (!empty($name->answer)){
                  if (strlen($name->answer)==14){
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $answer= $name->answer;
             $answer = str_replace( "-","", $answer);
            $answer = str_replace( " ","", $answer);
             $table=$que->value;
             $x=$que->x;
           $page->drawText($answer[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[1], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[2], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($answer[3], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[4], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[5], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($answer[6], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[7], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[8], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($answer[9], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($answer[10], $x, $que->y, 'UTF-8');
             $x=$x+$table;
              }}
         }
          if ($que->type=='date'){
              if (!empty($name->answer)){
                  if (strlen($name->answer)==10){
             $rt = preg_split('/\./',$name->answer,-1,PREG_SPLIT_NO_EMPTY);
//             dd($name->answer[6] . $name->answer[7] . $name->answer[8] . $name->answer[9]);
             list($year, $month, $day) =  explode('-', $name->answer);
            // dd($day, $month, $year);
//             $rt = explode('.', $name->answer);
//             dd($rt[2][]);
            // dd($rt);
             $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $table=$que->value;
             $x=$que->x;
             $page->drawText($day[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($day[1], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($month[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($month[1], $x, $que->y, 'UTF-8');
             $x=$x+29;
             $page->drawText($year[0], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[1], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[2], $x, $que->y, 'UTF-8');
             $x=$x+$table;
             $page->drawText($year[3], $x, $que->y, 'UTF-8');
             $x=$x+$table;
//             foreach ($rt as $word){
//             $page = $pdf->pages[$que->page];
//             $font = Font::fontWithPath('arial.ttf');
//             $page->setFont($font, 10);
//             $page->drawText($word, $x, $que->y, 'UTF-8');
//             $x=$x+$table+0.1;
//             }
          }}}
          if ($que->type=='check'){
         if ($que->value<>''){
             if ($que->value==$name->answer){
                 $page = $pdf->pages[$que->page];
             $font = Font::fontWithPath('arial.ttf');
             $page->setFont($font, $que->font);
             $page->drawText('v', $que->x, $que->y, 'UTF-8');
             
             }
         } else {
             
         }
         }
     }
    
//   $page->drawText($name->answer, 175, $height, 'UTF-8');
//   $height = $height - 10;
}
//$page->drawText($item->lasttname, 175, 725, 'UTF-8');
//$page->drawText($item->firstname, 175, 715, 'UTF-8');
//$page->drawText($item->sirtname, 175, 705, 'UTF-8');
$pdf->save('zend.pdf');
return \Response::make($pdf->render(), 200, [
            'Content-Type' => 'application/pdf',
            'Content-Disposition' => 'inline; filename="' . $filename . '"'
        ]);
    }
    public function getModalDownload(Request $request) {
        $clients = list_clients::where('id','=',$request->id)->get();
        foreach ($clients as $client){
        $bankIds = explode(' ',$client->bank);
        }
        foreach ($bankIds as $bank){
          $banks = bank::where('id','=',$bank)->get();
          foreach ($banks as $bankss){
              $string = $bankss->string;
               $name = $bankss->name;
          }
          echo "<a href=\"/get";
          echo $name;
          echo "/"; 
          echo $request->id;
          echo "\" target=\"_blanc\" download=\"\">" ;
          echo $string;
          echo "</a>";
          echo "<br>";
        }
        
    }
    public function getModalSee(Request $request) {
        $clients = list_clients::where('id','=',$request->id)->get();
        
        foreach ($clients as $client){
        $bankIds = explode(' ',$client->bank);
        $FIO = $this->translit($client->FIO);
        }
       // dd(csrf_token());
        
        foreach ($bankIds as $bank){
          $banks = bank::where('id','=',$bank)->get();
        }
//          foreach ($banks as $bankss){
//              $string = $bankss->string;
//              $name = $bankss->name;
//          } //echo $request->id;
//           $users=User::all();
        return view('see',['bankIds'=>$bankIds, 'clients'=>$clients,'id'=>$request->id, 'FIO'=>$FIO] );
//          echo "<form method=\"POST\" action=\"/get";
//          
//          echo $name;
//          echo "/"; 
//          echo $FIO;
//          echo "\" target=\"_blanc\">" ;
//          echo $string;
//          echo "<input type=\"hidden\" name=\"_token\" content=\"";
//          echo csrf_token();
//          echo "\" >";
//          echo "<input type=\"hidden\" id=\"";
//          echo $request->id;
//          echo "\" ";
//          echo "name=\"";
//          echo $request->id;
//          echo "\" >";
//          echo "<br>";
//          echo "<input type=\"submit\" value=\"";
//          echo $string;
//          echo "\" >";
//          echo "</form>";
//        }
        
    }
    public function getUserId(){
        $idUser = 1;
        $item = list_clients::orderBy('id')->get();
         foreach ($item as $items){
            $idUser = $items -> id;
         }
         
         return $idUser;
    }
    
    public function getBanksId(){
        $bankIDs = [];
        //dd($bankIDs);
        $bankID = !empty($_POST['bank']) ? $_POST['bank']:$this->getNullBank();
             foreach ($bankID as $id => $_) {
                 $bankIDs[] = $id;
             }
             
           // dd($bankIDs);
             return $bankIDs;
    }
    
    public function getNullBank($idUser){
      $banks=Client::where('id_user','=',$idUser)
              ->groupBy('id_bank')
              ->get();
      return $banks;
    }
    public function rc(){
      $clients = client::where('id_reg','=',Auth::id());
      $countclients = $clients->count(\DB::raw('DISTINCT id_user'));
     // dd($a);
      return view('rc',['countclients'=>$countclients]);
    }
    
    public function lists(){
      $roles = user_role::where('user_id','=',Auth::id())->get();
      foreach ($roles as $role){
          if ($role->role_id==1){
              $this->list_users ();
              echo ('1');
          } else {
              $this->list_clients();
              echo ('2');
          }
      } 
      
      
    }
    
    public function delete(Request $request){
        list_clients::where('id','=',$request->id)->delete();
        Client::where('id_user','=',$request->id)->delete();
        $this->list_clients();
    }
    public function edit_clients($id,Request $request){
         // Получаем id клиента. Если существует - работаем с ним, если нет, заводим новый
        $idUser = !empty($_POST['idUser']) ? $_POST['idUser'] : $id;
       //Нужны ли вопросы для созаемщика
       // dd($idUser);
        //dd($_POST);
        //Получаем ответы на вопросы
        $q = !empty($_POST['q']) ? $_POST['q'] : '0';
        //Получаем текущий шаг
        $save = !empty($_POST['save']) ? $_POST['save'] : '0';
        $step = !empty($_POST['step']) ? $_POST['step'] : '0';
        $answers = !empty($_POST['answers']) ? $_POST['answers'] : '0';
        $mas_quest = Client::where('id_user','=',$idUser)->orderBy('id_quest')->get();
        $queIDs = [];
        foreach ($mas_quest as $quest) {
           $queIDs[] = $quest->id_quest;
        }
        if ($q==0) {
          $answer = Client::where('id_user','=',$idUser)->wherein('id_quest', $queIDs)->get();  
        }   else {
            $answer = Client::where('id_user','=',$idUser)->wherein('id_quest', $queIDs)->get();
            foreach ($answers as $key => $_){
                if (isset($q[$key])) {
                $answers[$key] = $q[$key];
                }
            }
           // dd($answers);
          foreach ($answer as &$ans){
             
              $ans->answer = $answers[$ans->id_quest];
             // dd($ans->answer);
             
          }
          unset($ans);
          // dd($answer , $q);
          
        }
         if ($save>0){
             
             
             foreach ($answers as $key => $_){
                 $cli = Client::updateOrInsert(['id_user' => $idUser,'id_reg' => Auth::id(),'id_quest' => $key],['answer' => $_]);
             }   
             return $this->list_clients();
         } 

         $step++;
         $startQuest=($step-1)*20;
         

         

         
         $quest = Questions::wherein('id', $queIDs)->orderBy('position')->offset($startQuest)->limit(20)->get();
        
         if (count($quest)>0){
          //   dd($answer);
         return view('steped',['quest'=>$quest,'answer'=>$answer,'idUser'=>$idUser,'step'=>$step,'que'=>$q]);
        
         } else {
            
         } 
    }
    
    public function step(Request $request){
         // Получаем id клиента. Если существует - работаем с ним, если нет, заводим новый
        $idUser = !empty($_POST['idUser']) ? $_POST['idUser'] : $this->getUserId();
       //Нужны ли вопросы для созаемщика
        $dop = !empty($_POST['dop']) ? $_POST['dop'] : '0';
        //Получаем ответы на вопросы
        $q = !empty($_POST['q']) ? $_POST['q'] : '0';
        //Получаем текущий шаг
        $zhk = !empty($_POST['zhk']) ? $_POST['zhk'] : '0';
        
        
        $step = !empty($_POST['step']) ? $_POST['step'] : '0';
//dd($_POST);
        
     
         if ($zhk=='0'){
             foreach ($_POST['q'] as $id => $value){
                 $cli = Client::updateOrCreate(['id_user' => $idUser,'id_reg' => Auth::id(),'id_quest' => $id],['answer' => $value]);
             }   
         } else {
             $FIO = !empty($_POST['name']) ? $_POST['name'] : '';
            
              if (isset($FIO)){
             //dd($_POST);
           $list = new list_clients();
           $list->id_reg = Auth::id();
           $list->pub = 0;
           $list->FIO = !empty($_POST['name']) ? $_POST['name'] : '0';
           $ds = explode(' ',$list->FIO);
          // dd($ds);
           $list->zhk = $_POST['zhk'];
           $list->zastroy = $_POST['zastroy'];
           $list->comment = $_POST['comment'];
           $dop = !empty($_POST['dop']) ? $_POST['dop'] : '0';
           if ($dop=='on'){
               $list->dop = 1;
           } else {
               $list->dop = 0;
           }
           //$list->dop = !empty($_POST['dop']) ? $_POST['dop'] : '0';
           $bank = '';
           //dd($_POST['bank']);
           foreach ($_POST['bank'] as $id => $_) {
               $bank=$bank . ' ' . $id;
           }
           $bank = mb_substr($bank,1);
           //
            $list->bank = $bank;
            //dd($list);
            $list->save();
            $idUser = $this->getUserId();
           // dd($idUser);
           //  $list = list_clients::firstOrNew(['id_user' => $idUser, 'id_reg' => Auth:id(), 'FIO' => '','zhk' => '','zastroy' => '','comment' => '','bank' => '', 'dop' => '']);
         $ids=1;
         
         foreach ($ds as $d){
            $cli = Client::updateOrCreate(['id_user' => $this->getUserId(),'id_reg' => Auth::id(),'id_quest' => $ids],['answer' => $d]);
         $ids++;
         }
         }
         }
         $step++;
         $startQuest=($step-1)*20;
         
        
         $mas_quest= bank_quest::wherein('id_bank', $this->getBanksId())->get();
         $queIDs = [];
             foreach ($mas_quest as $quest) {
                 $queIDs[] = $quest->id_quest;
             }
          //dd($bank);
             if ($dop=='on'){
                 $answer = Client::where('id_user','=',$idUser)->wherein('id_quest', $queIDs)->offset($startQuest)->limit(20)->get();
                 $quest = Questions::wherein('id', $queIDs)->orderBy('position')->offset($startQuest)->limit(20)->get();
             } else {
                 //$answer = Client::firstOrNew(['id_user'=>1])->wherein('id_quest', $queIDs)->offset($startQuest)->limit(20)->get();
                 
                 $answer = Client::where('id_user','=',$idUser)->wherein('id_quest', $queIDs)->offset($startQuest)->limit(20)->get();
                // dd($answer);
                 $quest = Questions::wherein('id', $queIDs)->where('dop','=',0)->orderBy('position')->offset($startQuest)->limit(20)->get();
             }

        
         if (count($quest)>0){
         return view('step',['quest'=>$quest,'answer'=>$answer,'dop'=>$dop,'idUser'=>$idUser,'step'=>$step,'bankIDs'=>$this->getBanksId()]);
         } else {
             $cli = list_clients::find($idUser);
             $cli -> pub = 1;
             $cli->save();
            return $this->list_clients();
         } 
    }
        
    
}

