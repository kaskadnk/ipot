<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace App\Http\Middleware;
use Closure;

class OwnerMiddleware {
    public function handle($request, Closure $next, $role){
        if (!$request->user()->hasRole($role)){
            return redirect('/');
        }
        return $next($request);
    }
}
