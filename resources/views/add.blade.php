
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Создание анкеты</h3>
            </div>
             <form role="form" class="form-horizontal" id="forma">
            <div class="box-body">
                
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <input type="hidden" name="_method" value="POST">
            <div class="form-group has-feedback">
                <label for="q" class="control-label col-xs-3">ФИО</label>
                <div class="col-xs-6">
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" required type="text" class="form-control"   id="name"  name="name" placeholder="ФИО"  data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
           </div> 
            <div class="form-group has-feedback">
                <label for="zhk" class="control-label col-xs-3">ЖК</label>
                <div class="col-xs-6">
            <select  name="zhk" class="form-control">
                @foreach ($zhks as $zhk)
                
                  <option>{{$zhk->name}}</option>
                  
                  @endforeach
            </select>
                    
            </div>
           </div> 
            <div class="form-group has-feedback">
                <label for="zastroy" class="control-label col-xs-3">Застройщик</label>
                <div class="col-xs-6">
            <select  name="zastroy" class="form-control">
                @foreach ($zastroys as $zastroy)
                
                  <option>{{$zastroy->name}}</option>
                  
                  @endforeach
            </select>
                    
            </div>
           </div> 
            <div class="form-group has-feedback">
                <label for="comment" class="control-label col-xs-3">Комментарий</label>
                <div class="col-xs-6">
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()"  type="text" class="form-control"   id="comment"  name="comment" placeholder="Комментарий"  data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
           </div> 
            <div class="form-group has-feedback">
                <label for="q" class="control-label col-xs-3">Банки</label>
                <div class="col-xs-6">
                    <div class="input-group">
            @foreach ($item as $bank)
            
            <label>
                  <input type="checkbox"  class="minimal bank" name="bank[{{$bank->id}}]">
                  {{$bank->string}}
                </label>
            <br>
            @endforeach
            </div>
                <span class="glyphicon form-control-feedback check"></span>
    </div>
           </div>
            <div class="form-group has-feedback">
                <label for="q" class="control-label col-xs-3">Созаемщик</label>
                <div class="col-xs-6">
                    <div class="input-group">
            
            
            <label>
                  <input type="checkbox" class="minimal" name="dop">
                  Имеется
                </label>
            <br>
            
            </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
           </div>
            
            </div>
                 
         </form>
           
        <div class="box-footer">
                <button id="" type="submit" onclick="getquest()" class="btn btn-primary">Создать анкету</button>
              
              </div>
        
                
</div>
