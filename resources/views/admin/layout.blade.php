<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Приложение</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  
  <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="/bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="/bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
 <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="../../plugins/iCheck/all.css">

  <!-- Select2 -->
  <link rel="stylesheet" href="../../bower_components/select2/dist/css/select2.min.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="../../bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="../../dist/css/skins/_all-skins.min.css">

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
    <?php $roles = $role->role_id; ?>
                           <input type="hidden" id="roles"  name="roles"  value="<?php echo $roles; ?>" >
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  align="center" >
       
        <div class="modal-title h4" id="myModalLabel" style="font: bold 32px/37px 'Roboto',sans-serif; color: #154189;">Ссылки для скачивания</div>
      </div>
      <div class="modal-body" align="center" >
        
      </div>
    </div>
  </div>
</div>
     <div class="modal fade" id="myModalSee" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header"  align="center" >
       
        <div class="modal-title h4" id="myModalLabel" style="font: bold 32px/37px 'Roboto',sans-serif; color: #154189;">Ссылки для просмотра</div>
      </div>
      <div class="see modal-body" align="center" >
        
      </div>
    </div>
  </div>
</div>
<div class="wrapper">
     @if(session('message'))
        <div class="alert alert-info alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <p>{{ session('message') }}</p>
        </div>
    @endif
    @include('admin.header.header')
    @include('admin.sidebar.sidebar')
    
      <div class="content-wrapper" id="result_div">
        
          

      </div>
    </section
        <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->
 <meta name="_token" content="{!! csrf_token() !!}" />
      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form><!-- ./wrapper -->

<!-- jQuery 3 -->

<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="../../plugins/input-mask/jquery.inputmask.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="../../plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="../../bower_components/moment/min/moment.min.js"></script>
<script src="../../bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="../../bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="../../bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="../../plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="../../bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="../../dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="../../dist/js/demo.js"></script>
<script src="../../bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="../../bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="../../bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- Page script -->
<script>
    

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()
    
   
    

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'dd-mm-yyyy'
    })

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
    //Red color scheme for iCheck
    $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
      checkboxClass: 'icheckbox_minimal-red',
      radioClass   : 'iradio_minimal-red'
    })
    //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    //Timepicker
    $('.timepicker').timepicker({
      showInputs: false
    })
  })
</script>
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>
<script src="../../bower_components/select2/dist/js/select2.full.min.js"></script>
<script>
function list() {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/list",
        //  data: {"date":date, "napr":napr},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          },
          complete: function() {
              $("#example2").dataTable({	
		

  "aoColumnDefs":[{
        "sTitle":"Название сайта"
      , "aTargets": [ "site_name" ]
  },{
        "aTargets": [ 0 ]
        ,"type": "ruDate"
      
     
  },{
        "aTargets": [ 4 ]
      , "bSortable": false
     
  },{
        "aTargets": [ 5 ]
      , "bSortable": false
     
  },{
        "aTargets": [ 6 ]
      , "bSortable": false
     
  }],
  
   "language": {
      "processing": "Подождите...",
      "search": "Поиск:",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Следующая",
        "last": "Последняя"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }
  }
});
          }
      })
      
}

function searchis() {
    search = $('input[name="search"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/search",
            data: {"search":search},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}


function add() {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/add",
        data: jQuery("form").serializeArray(),
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
             
          },
          complete: function() {
              jQuery(function($){
              $('input[type="tel"]').mask("999-99-9999");
              });
              jQuery(function($){
              $('input[type="snils"]').mask("999-999-999 99");
              });
          }
      })
      
}

function maskget() {
    jQuery(function($){
              $('input[type="tel"]').mask("+7(999)999-99-99");
              });
              jQuery(function($){
              $('input[type="snils"]').mask("999-999-999 99");
              });
}
function lists() {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/lists",
        //  data: {"date":date, "napr":napr},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          },
          complete: function() {
              $('.select2').select2();
          }
      })
}
function create() {
    $(function() {
    //при нажатии на кнопку с id="save"
    
   
      //переменная formValid
      var formValid = true;
      //перебрать все элементы управления input 
      $('input').each(function() {
      //найти предков, которые имеют класс .form-group, для установления success/error
      var formGroup = $(this).parents('.form-group');
      //найти glyphicon, который предназначен для показа иконки успеха или ошибки
      var glyphicon = formGroup.find('.form-control-feedback');
      //для валидации данных используем HTML5 функцию checkValidity
      if (this.checkValidity()) {
        //добавить к formGroup класс .has-success, удалить has-error
        formGroup.addClass('has-success').removeClass('has-error');
        //добавить к glyphicon класс glyphicon-ok, удалить glyphicon-remove
        glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
      } else {
        //добавить к formGroup класс .has-error, удалить .has-success
        formGroup.addClass('has-error').removeClass('has-success');
        //добавить к glyphicon класс glyphicon-remove, удалить glyphicon-ok
        glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
        //отметить форму как невалидную 
        formValid = false;  
      }
    });
    //если форма валидна, то
    if (formValid) {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/create",
        //  data: {"date":date, "napr":napr},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          },
          complete: function() {
              $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
      checkboxClass: 'icheckbox_minimal-blue',
      radioClass   : 'iradio_minimal-blue'
    })
          }
      })
  }
    });
}
function getquest() {
     $(function() {
    //при нажатии на кнопку с id="save"
    
   
      //переменная formValid
      var formValid = true;
      var check = false;
     
      //перебрать все элементы управления input 
      $('input').each(function() {
      //найти предков, которые имеют класс .form-group, для установления success/error
      var formGroup = $(this).parents('.form-group');
      //найти glyphicon, который предназначен для показа иконки успеха или ошибки
      var glyphicon = formGroup.find('.form-control-feedback');
      //для валидации данных используем HTML5 функцию checkValidity
      
      if (this.checkValidity()) {
        //добавить к formGroup класс .has-success, удалить has-error
        formGroup.addClass('has-success').removeClass('has-error');
        //добавить к glyphicon класс glyphicon-ok, удалить glyphicon-remove
        glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
      } else {
        //добавить к formGroup класс .has-error, удалить .has-success
        formGroup.addClass('has-error').removeClass('has-success');
        //добавить к glyphicon класс glyphicon-remove, удалить glyphicon-ok
        glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
        //отметить форму как невалидную 
        formValid = false;  
      }
    });
         $('.bank').each(function() {
        if (this.checked){ check = true;
        }
      })
      if (check===false) {
          $('.check').each(function() {
              alert('Отметьте хоть один банк');
               var formGroup = $(this).parents('.form-group');
      //найти glyphicon, который предназначен для показа иконки успеха или ошибки
      var glyphicon = formGroup.find('.form-control-feedback');
             formGroup.addClass('has-error').removeClass('has-success');
        //добавить к glyphicon класс glyphicon-remove, удалить glyphicon-ok
        glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
        //отметить форму как невалидную 
        formValid = false;  
          })
      }
    //если форма валидна, то
    if (formValid) {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/step",
        data: jQuery("form").serializeArray(),
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
  }
     })
}

function edit_profil() {
    // date = $('input[name="date"]').val();
	//napr = $('select[name="napr"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
     $.ajax({
          type:"POST",
          url:"/edit_profil",
        data: jQuery("form").serializeArray(),
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}


function stepp() {
 $(function() {
    //при нажатии на кнопку с id="save"
    
   
      //переменная formValid
      var formValid = true;
      //перебрать все элементы управления input 
      $('input').each(function() {
      //найти предков, которые имеют класс .form-group, для установления success/error
      var formGroup = $(this).parents('.form-group');
      //найти glyphicon, который предназначен для показа иконки успеха или ошибки
      var glyphicon = formGroup.find('.form-control-feedback');
      //для валидации данных используем HTML5 функцию checkValidity
      if (this.checkValidity()) {
        //добавить к formGroup класс .has-success, удалить has-error
        formGroup.addClass('has-success').removeClass('has-error');
        //добавить к glyphicon класс glyphicon-ok, удалить glyphicon-remove
        glyphicon.addClass('glyphicon-ok').removeClass('glyphicon-remove');
      } else {
        //добавить к formGroup класс .has-error, удалить .has-success
        formGroup.addClass('has-error').removeClass('has-success');
        //добавить к glyphicon класс glyphicon-remove, удалить glyphicon-ok
        glyphicon.addClass('glyphicon-remove').removeClass('glyphicon-ok');
        //отметить форму как невалидную 
        formValid = false;  
      }
    });
    //если форма валидна, то
    if (formValid) {
        step = $('input[name="step"]').val();
    idUser = $('input[name="idUser"]').val();
    startQuest = $('input[name="startQuest"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий
    answer = jQuery("form").serialize(); 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          

          type:"POST",
          url:"/step",
        data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      });
    }
  
});

  
}

function edit(id) {
   // step = $('input[name="step"]').val();
   // idUser = $('input[name="idUser"]').val();
   // startQuest = $('input[name="startQuest"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий
   // answer = jQuery("form").serialize(); 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          type:"POST",
          url:"/edit/"+ id,
        data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}
function editsave(id) {
  save = $('input[name="save"]').val();
   
    save=1;
    
    var  HF = document.getElementById('save');
     HF.value = save;
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          type:"POST",
          url:"/edit/"+ id,
        data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}
function download(id) {
    
 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
       
     $.ajax({
          type:"POST",
          url:"/getModalDownload",
        data: {"id":id},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('.modal-body').html(responce);
               $("#myModal").modal('show');
          }
      })
}
function see(id) {
    
 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
       
     $.ajax({
          type:"POST",
          url:"/getModalSee",
        data: {"id":id},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('.see').html(responce);
               $("#myModalSee").modal('show');
          }
      })
}
function del(id) {

     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
       
     $.ajax({
          type:"POST",
          url:"/del",
        data: {"id":id},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
             // $('div#result_div').html(responce);
             list();
          }
      })
}
function stepd() {
    step = $('input[name="step"]').val();
    if (step<=1) {
    
        step = 0;
    } else {step= step-2;}
    var  HF = document.getElementById('step');
     HF.value = step;
    step = $('input[name="step"]').val();
    idUser = $('input[name="idUser"]').val();
    startQuest = $('input[name="startQuest"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий
    answer = jQuery("form").serialize(); 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          type:"POST",
          url:"/step",
        data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}
function editd(id) {
    step = $('input[name="step"]').val();
   
    if (step<=1) {
     
        step = 0;
    } else {step= step-2;}
    
    var  HF = document.getElementById('step');
     HF.value = step;
    step = $('input[name="step"]').val();
    idUser = $('input[name="idUser"]').val();
    startQuest = $('input[name="startQuest"]').val();
     //Отправляем ajax-запрос к файлу newsfilter.php, в нем передаем дату и массив категорий
    answer = jQuery("form").serialize(); 
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          type:"POST",
          url:"/edit/"+id,
        data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}

function rc() {
  
     $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
         tagsArray = new Array();
         $('input[name="q"]').each(function() {tagsArray.push(this.value);});
     $.ajax({
          type:"POST",
          url:"/rc",
       // data: jQuery("form").serializeArray(),//{"step":step, "idUser":idUser, "startQuest":startQuest,"answer":answer},
          cache: false,
          success: function(responce){ 

               //Загружаем результат в блок с id=result_div и прячем картинку загрузки
               $('div#result_div').html(responce);
              
          }
      })
}
</script>

<script>
function yesChange(){
document.getElementById("78").value=document.getElementById("69").value;
document.getElementById("80").value=document.getElementById("71").value;
document.getElementById("81").value=document.getElementById("72").value;
document.getElementById("82").value=document.getElementById("73").value;
document.getElementById("83").value=document.getElementById("74").value;
document.getElementById("85").value=document.getElementById("76").value;
        };
        
        function yesChange1(){
document.getElementById("333").value=document.getElementById("324").value;
document.getElementById("335").value=document.getElementById("326").value;
document.getElementById("336").value=document.getElementById("327").value;
document.getElementById("337").value=document.getElementById("328").value;
document.getElementById("338").value=document.getElementById("329").value;
document.getElementById("340").value=document.getElementById("331").value;
        };

  $(function () {
    roles = $('input[name="roles"]').val();  
    if (roles==='1') {
     lists();   
    } else {
  list();
    }
  })
  jQuery.extend( jQuery.fn.dataTableExt.oSort, {
		"ruDate-asc": function ( a, b ) {
			var ruDatea = $.trim(a).split('.');
			var ruDateb = $.trim(b).split('.');

			if(ruDatea[2]*1 < ruDateb[2]*1)
				return 1;
			if(ruDatea[2]*1 > ruDateb[2]*1)
				return -1;
			if(ruDatea[2]*1 == ruDateb[2]*1)
				{
				if(ruDatea[1]*1 < ruDateb[1]*1)
					return 1;
				if(ruDatea[1]*1 > ruDateb[1]*1)
					return -1;
				if(ruDatea[1]*1 == ruDateb[1]*1)
					{
					if(ruDatea[0]*1 < ruDateb[0]*1)
						return 1;
					if(ruDatea[0]*1 > ruDateb[0]*1)
						return -1;
					}
					else
						return 0;
				}				
		},
 
		"ruDate-desc": function ( a, b ) {
			var ruDatea = $.trim(a).split('.');
			var ruDateb = $.trim(b).split('.');
			
			if(ruDatea[2]*1 < ruDateb[2]*1)
				return -1;
			if(ruDatea[2]*1 > ruDateb[2]*1)
				return 1;
			if(ruDatea[2]*1 == ruDateb[2]*1)
				{
				if(ruDatea[1]*1 < ruDateb[1]*1)
					return -1;
				if(ruDatea[1]*1 > ruDateb[1]*1)
					return 1;
				if(ruDatea[1]*1 == ruDateb[1]*1)
					{
					if(ruDatea[0]*1 < ruDateb[0]*1)
						return -1;
					if(ruDatea[0]*1 > ruDateb[0]*1)
						return 1;
					}
					else
						return 0;
				}
		}
	});
</script>
</body>
</html>