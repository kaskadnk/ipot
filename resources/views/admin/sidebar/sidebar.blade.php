  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <span class="hidden-xs">{{ Auth::user()->partner }}</span>
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- search form -->
      
      <form action="#"  class="sidebar-form">
        <div class="input-group">
          <input type="text" name="search" class="form-control" oninput="searchis()" placeholder="Поиск...">
          </form>
          <span class="input-group-btn">
              
                <button onclick="searchis()" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Навигация</li>
        @if(Auth::user()->hasRole('admin')){
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-user-circle"></i> <span>Пользователи</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="#" onclick="lists()"><i class="fa fa-circle-o"></i> Список пользователей</a></li>
            <li><a href="#" onclick="add()"><i class="fa fa-circle-o"></i> Создание пользователей</a></li>
          </ul>
        </li>
        }
        @endif
        @if (Auth::user()->hasRole('manager')){
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-address-book"></i>
            <span>Клиенты</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#" onclick="list()"><i class="fa fa-circle-o"></i> Список клиентов</a></li>
            <li><a href="#" onclick="create()"><i class="fa fa-circle-o"></i> Создать анкету</a></li>
          </ul>
        </li>
        }
        @endif
        <li class="treeview">
          <a href="#">
            <i class="fa fa-files-o"></i> <span>Отчеты</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              @if (Auth::user()->hasRole('admin')){
            <li class="active"><a href="index.html"><i class="fa fa-circle-o"></i> Отчет по партнерам</a></li>
            } @endif
            @if (Auth::user()->hasRole('manager')){
            <li><a href="#" onclick="rc()"><i class="fa fa-circle-o"></i> Отчет по клиентам</a></li>
            } @endif
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>