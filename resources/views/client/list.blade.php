<section class="content">
      <div class="row">
        <div class="col-xs-12">
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Клиенты</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                    <th>Дата</th>
                  <th>ФИО</th>
                  <th>ЖК</th>
                  <th>Застройщик</th>
                  <th>Комментарий</th>
                  <th>Редактировать</th>
                  <th>Просмотр</th>
                  <th>Скачать</th>
                  <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                 
                 @foreach($clients as $client)
                 <tr>
                     <td>{{ Carbon\Carbon::parse($client->created_at)->format('d.m.Y')}}</td>
                     <td>
                 
                {{$client->FIO}}
                  
                  
                
                
                </td>
                <td>{{$client->zhk}}</td>
                 <td>{{$client->zastroy}}</td>
                 <td>{{$client->comment}}</td>
                 <td><input type="button" class="form-control" onclick="edit({{$client->id}})" name="{{$client->id}}" value="Редактировать"></td>
                 <td><input type="button" class="form-control" onclick="see({{$client->id}})" name="{{$client->id}}" value="Просмотр"></td>
                  <td><input type="button" class="form-control" onclick="download({{$client->id}})" name="{{$client->id}}" value="Скачать"></td>
                
                     <td><input type="button" class="form-control" onclick="del({{$client->id}})" name="{{$client->id}}" value="Удалить"></td>
                </tr>
                @endforeach
                                                                 </tbody>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          
        </div>
      </div>
</section>
          