
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Создание анкеты</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <form action="/step/2" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            @foreach ($quest as $q)
           {{$q->quest}}
           <br>
           @if ($q->type=='string')
            <input type="text" name="q{{$q->id}}" value=""><br>
            @endif
            @if ($q->type=='date')
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="text" name="q{{$q->id}}" class="form-control pull-right" id="datepicker">
                </div>
            @endif
            @if ($q->type=='enum')
            <?php $arr = explode(',', $q->answer); ?>
            
            <select  name="q{{$q->id}}" style="width: 30%;">
                @foreach ($arr as $answer)
                $answer = trim($answer);
                  <option>{{$answer}}</option>
                  
                  @endforeach
            </select><br>
            @endif
            @endforeach
            
            
            <input type="submit">
        </form>
        
                </div>
</div>
