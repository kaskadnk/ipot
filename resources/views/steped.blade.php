
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Редактирование анкеты</h3>
            </div>
            <!-- /.box-header -->
            
                <form role="form" class="form-horizontal" id="forma">
                    <div class="box-body">
             <input type="hidden" name="idUser" value="{{$idUser}}"> 

             <input type="hidden" name="step" id="step" value="{{$step}}">
             <input type="hidden" name="save" id="save" value="0">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            
            @foreach ($answer as $answ)
           
            <input type="hidden" name="answers[{{$answ->id_quest}}]"  value="{{$answ->answer}}">
            
            @endforeach
            
           <?php $start=0; ?>
            
            @foreach ($quest as $q)
                       

           <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($q->id==$answ->id_quest)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
           
           <?php $req=$q->required; ?>
            <?php 
            $questions = [69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340];
            //dd($answer);
            ?>
             @if ($q->id==69)
           
           
           <div class="form-group has-feedback">
               <label for="q[{{$q->id}}]" class="control-label col-xs-12">Адрес по регистрации</label>
               <div class="col-xs-4">
                   <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==69)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="69"  name="q[69]" placeholder="Область" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==71)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="71"  name="q[71]" placeholder="Город" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==72)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="72"  name="q[72]" placeholder="Улица" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
               </div><br>&nbsp<br>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==73)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="73"  name="q[73]" placeholder="№ дома" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==74)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="74"  name="q[74]" placeholder="Корпус" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==76)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="76"  name="q[76]" placeholder="Кваритра" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
                </div> 
              
           
           
           <div class="form-group has-feedback">
               <label for="q[{{$q->id}}]" class="control-label col-xs-12">Адрес фактического местонахождения</label>
               
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==78)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="78"  name="q[78]" placeholder="Область" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==80)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="80"  name="q[80]" placeholder="Город" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==81)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="81"  name="q[81]" placeholder="Улица" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
   </div><br>&nbsp<br>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==82)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="82"  name="q[82]" placeholder="№ дома" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==83)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="83"  name="q[83]" placeholder="Корпус" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==85)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="85"  name="q[85]" placeholder="Кваритра" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
                </div> 
               @endif
                @if ($q->id==324)
           
           
           <div class="form-group has-feedback">
               <label for="q[{{$q->id}}]" class="control-label col-xs-12">Адрес по регистрации</label>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==324)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="324"  name="q[324]" placeholder="Область" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==326)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="326"  name="q[326]" placeholder="Город" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==327)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="327"  name="q[327]" placeholder="Улица" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
               </div><br>&nbsp<br>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==328)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="328"  name="q[328]" placeholder="№ дома" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==329)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="329"  name="q[329]" placeholder="Корпус" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==331)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="331"  name="q[331]" placeholder="Кваритра" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
                </div> 
              
           
           
           <div class="form-group has-feedback">
               <label for="q[{{$q->id}}]" class="control-label col-xs-12">Адрес фактического местонахождения</label>
               
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==333)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="333"  name="q[333]" placeholder="Область" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==335)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="335"  name="q[335]" placeholder="Город" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==336)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="336"  name="q[336]" placeholder="Улица" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
   </div><br>&nbsp<br>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==337)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="337"  name="q[337]" placeholder="№ дома" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==338)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="338"  name="q[338]" placeholder="Корпус" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
               <div class="col-xs-4">
                    <?php $req=$q->required; ?>
            @foreach ($answer as $answ)
           @if ($answ->id_quest==340)
           <?php $ans=$answ->answer ?>
       
           @endif
           @endforeach
                    <div class="input-group">
                    <span class="input-group-addon"><i class="glyphicon "></i></span> 
                       <input onClick="maskget()" {{$q->required}} type="text" class="form-control"   id="340"  name="q[340]" placeholder="Кваритра" minlength="{{$q->minlength}}" maxlength="{{$q->maxlength}}"  value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
                </div> 
               @endif
            @if (array_search($q->id, $questions) === false)
            
            
           @if ($q->type=='string')
          
           <div class="form-group has-feedback">
                <label for="q[{{$q->id}}]" class="control-label col-xs-3">{{$q->quest}}</label>
                <div class="col-xs-6">
                    <div class="input-group"><?php // dd($q->id); ?>
                       <input onClick="maskget()" {{$q->required}} type="{{$q->input}}" class="form-control"   id="q[{{$q->id}}]"  name="q[{{$q->id}}]" placeholder="{{$q->quest}}" value="{{$ans or ''}}" data-mask>
                </div>
                <span class="glyphicon form-control-feedback"></span>
    </div>
           </div> 
               @endif
            @if ($q->type=='date')
            <div class="form-group has-feedback">
                <label for="q[{{$q->id}}]" class="control-label col-xs-3">{{$q->quest}}</label>
                <div class="col-xs-3">
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input  type="date" name="q[{{$q->id}}]" class="form-control" id="datepicker" value="{{$ans or ''}}">
                </div>
                    <span class="glyphicon form-control-feedback"></span>
    </div>
           </div> 
            @endif
            @if ($q->type=='enum')
            
            <?php $arr = explode(',', $q->answer); ?>
            <div class="form-group has-feedback">
                <label for="q[{{$q->id}}]" class="control-label col-xs-3">{{$q->quest}}</label>
                <div class="col-xs-3">
                    
            <select  name="q[{{$q->id}}]" class="form-control">
                @foreach ($arr as $answe)
                <?php $answe = trim($answe); ?>
                @if ($answe==$ans)
                
                <?php echo 'ds'; ?>
                  <option selected>{{$answe}}</option>
                @endif
                @if ($answe!=$ans)
                <?php echo 'dsds'; ?>
                  <option>{{$answe}}</option>
                @endif  
                  @endforeach
            </select>
            </div>
           </div> 
            @endif
            @endif
            <?php $start++; ?>
            @endforeach
            </div>
     
                </form>
      <div class="box-footer">
                <button id="" type="submit" onclick="editd({{$idUser}})" class="btn btn-primary">Назад</button>
                
             <?php if (count($quest)==20){ ?>
                   <button id="next" type="submit" onclick="edit({{$idUser}})" class="btn btn-primary">Вперед</button>
             <?php } ?>
                   <button id="save" type="submit" onclick="editsave({{$idUser}})" class="btn btn-primary">Сохранить</button>
              </div>
               
        
                
</div>
