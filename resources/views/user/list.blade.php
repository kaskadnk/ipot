
<div class="box">
            <div class="box-header">
              <h3 class="box-title">Пользователи</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                
              <table id="example2" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>Логин</th>
                  <th>ФИО</th>
                  <th>Партнер</th>
                  <th>email</th>
                  <th>Телефон</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($users as $user)
                <tr>
                  <td>{{$user->name}}</td>
                  <td>{{$user->firstname}} {{$user->lastname}} {{$user->sirname}}</td>
                  <td>{{$user->partner}}</td>
                  <td>{{$user->email}}</td>
                  <td>{{$user->phone}}</td>
                </tr>
                @endforeach
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
          