<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return view('welcome');
});
Route::group(['middleware'=>'auth'], function(){
    Route::resource('/admin','UserProfile');
    
    Route::group(['middleware'=>'role:admin'],function(){
        Route::any('/lists','UserProfile@list_users');
        Route::any('/add','UserProfile@add_user');
        Route::any('/edit_profil','UserProfile@edit_profil');
    });
    Route::group(['middleware'=>'role:manager'],function(){
        Route::any('/edit/{id}','UserProfile@edit_clients'); 
      Route::any('/getModalDownload','UserProfile@getModalDownload');
      Route::any('/getModalSee','UserProfile@getModalSee'); 
      Route::any('/del','UserProfile@delete'); 
      Route::any('/list','UserProfile@list_clients'); 
      Route::any('/rc','UserProfile@rc'); 
      Route::any('/getvtb/{id}','UserProfile@get_vtb');
      Route::any('/getspb/{id}','UserProfile@get_spb');
      Route::any('/getsber/{id}','UserProfile@get_sber');
       Route::any('/getprom/{id}','UserProfile@get_prom');
             Route::any('/search','UserProfile@search_clients');
      Route::any('/create','UserProfile@create');
      Route::any('/step','UserProfile@step');
      Route::any('/edit_profil','UserProfile@edit_profil');
    });
});

 Route::get('/home', 'HomeController@index');

Auth::routes();


